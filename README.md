# Shacket
A modern theme using high contrast colours and supports video.

## Home Page

### Banner Image
This image will be displayed at the top of the home page.

### Banner Title
This text will be displayed on the banner. 

### Banner sub-heading
This text will be displayed on the banner underneath the title (in a smaller font)

### Popular Categories Section Title
This title can be customised for the popular categories section. 
<br/> Default value is 'Popular Categories'.

### Featured collection in home page
Dropdown menu to select a collection to be featured by itself as a banner.

### Popular items section title
This title can be customised for the popular products section.
<br/> Default value is 'Popular Items'.

### Parallax Collection in home page
Dropdown menu to select a collection to be featured as a parallax collection.

### Video section image/thumbnail
This image will be displayed as a thumbnail for an optional video.

### Video section video youtube link
This text field takes a video link for an optional youtube video to be shown on the home page.

## Footer
### Footer: First Menu
_Note: Submenus items will not be displayed_

### Footer: Second Menu
_Note: Submenus items will not be displayed_

## Collection 
### Tags
#### show_in_home
This tag is used for collections to be shown in the home page in the popular categories section.

### Custom Fields
#### Featured Collection Image
Custom field to select a custom image to be set if the collection is selected as a featured collection

#### Parallax Collection Image
Custom field to select a custom image to be shown if the collection is chosen as a parallax collection.

## Product
### Tags
#### show_in_home
This tag is used for products to be featured in the home page in the popular items section.

### Custom Fields
#### Category
This drop down menu is used to put the product in a category, which is shown on the product card.