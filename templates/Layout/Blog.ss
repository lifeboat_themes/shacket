<% include Header %>
<main class="main">
    <nav class="breadcrumb-nav">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/"><i class="d-icon-home"></i></a></li>
                <li><a href="#" class="active">Blog</a></li>
            </ul>
        </div>
    </nav>
    <div class="page-content pb-10 mb-10">
        <div class="container">
            <div class="posts grid row">
                <% loop $PaginatedPages(6) %>
                    <div class="grid-item col-sm-6">
                        <article class="post post-grid">
                            <figure class="post-media overlay-zoom">
                                <a href="$AbsoluteLink">
                                    <% if $Image %>
                                        <img src="$Image.ScaleWidth(580).AbsoluteLink" alt="$Title" width="580" height="420" />
                                    <% else %>
                                        <img src="$SiteSettings.Logo.ScaleWidth(580).AbsoluteLink" alt="$Title" width="580" height="420" />
                                    <% end_if %>
                                </a>
                            </figure>
                            <div class="post-details">
                                <div class="post-meta">
                                    on <a href="#" class="post-date">$Created.format('MMM d Y')</a>
                                </div>
                                <h4 class="post-title"><a href="$AbsoluteLink">$Title</a></h4>
                                <p class="post-content">$Content.FirstParagraph.LimitCharactersToClosestWord(256)</p>
                                <a href="$AbsoluteLink" class="btn btn-link btn-underline btn-primary">Read more<i class="d-icon-arrow-right"></i></a>
                            </div>
                        </article>
                    </div>
                <% end_loop %>
            </div>
            <% if $PaginatedPages(6).MoreThanOnePage %>
                <ul class="pagination mt-10 pt-4 mb-10 pb-4 justify-content-center">
                    <% if $PaginatedPages(6).NotFirstPage %>
                        <li class="page-item">
                            <a class="page-link page-link-prev" href="$PaginatedPages(6).PrevLink" aria-label="Previous" tabindex="-1"
                               aria-disabled="true">
                                <i class="d-icon-arrow-left"></i>Prev
                            </a>
                        </li>
                    <% else %>
                        <li class="page-item disabled">
                            <a class="page-link page-link-prev" aria-label="Previous" tabindex="-1" aria-disabled="true">
                                <i class="d-icon-arrow-left"></i>Prev
                            </a>
                        </li>
                    <% end_if %>
                        <% loop $PaginatedPages(6).PaginationSummary %>
                            <% if $CurrentBool %>
                                <li class="page-item active" aria-current="page"><a class="page-link" href="#">$PageNum</a></li>
                            <% else %>
                                <% if $Link %>
                                    <li class="page-item"><a class="page-link" href="$Link">$PageNum</a></li>
                                <% else %>
                                    <li class="page-item">...</li>
                                <% end_if %>
                            <% end_if %>
                        <% end_loop %>
                    <% if $PaginatedPages(6).NotLastPage %>
                        <li class="page-item">
                            <a class="page-link page-link-next" href="$PaginatedPages(6).NextLink" aria-label="Next">
                                Next<i class="d-icon-arrow-right"></i>
                            </a>
                        </li>
                    <% else %>
                        <li class="page-item disabled">
                            <a class="page-link page-link-next" aria-label="Next" aria-disabled="true">
                                Next<i class="d-icon-arrow-right"></i>
                            </a>
                        </li>
                    <% end_if %>
                    </ul>
                <% end_if %>
        </div>
    </div>
</main>