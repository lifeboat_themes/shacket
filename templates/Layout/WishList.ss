<% include Header %>
<main class="main">
    <div class="page-content mb-10">
        <div class="container">
            <div class="row main-content-wrap gutter-lg mt-md-4 mb-md-4">
                <div class="main-content">
                    <div class="row cols-3 cols-sm-4 product-wrapper">
                        <% if $WishList.Products.count %>
                            <% loop $WishList.Products %>
                                <div class="product-wrap">
                                    <% include ProductCard Product=$Me %>
                                </div>
                            <% end_loop %>
                        <% else %>
                            <div class="col">
                                <p class="mt-md-4">You have no products in your wish list.</p>
                            </div>
                        <% end_if %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>